module main;

import core.sys.windows.dll;
import std.file;
import std.conv;
import std.string;
import std.math;

import eyemax.StrUtil;
import eyemax.WinAPI;

/// The hInstance of this DLL
__gshared HINSTANCE g_hInst;

/**
 * DLL Entrypoint
 * Params:
 * 	hInstance = A handle to the DLL module. The value is the base address of the DLL
 * 	ulReason = The reason code that indicates why the DLL entry-point function is being called
 * 	pvReserved = This value depends on the value of ulReason
 * Returns: A BOOL indicating success or failure
 */
extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved) {
	// Main DLL switch
	switch (ulReason) {
		case DLL_PROCESS_ATTACH:
			g_hInst = hInstance;
			dll_process_attach( hInstance, true );
			break;
		case DLL_PROCESS_DETACH:
			dll_process_detach( hInstance, true );
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach( true, true );
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach( true, true );
			break;
		default:
			break;
	}
	return true;
}

/**
 * WndProc hook procedure to handle WM_GETMINMAXINFO messages
 * Params:
 * 	hWnd = The handle to the window that we are currently processing
 * 	uMsg = The message we've been given
 * 	wParam = The contents of this parameter depend on the value of the uMsg parameter
 * 	lParam = The contents of this parameter depend on the value of the uMsg parameter
 * Returns: The return value is the result of the message processing and depends on the message sent
 */
extern(Windows) LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// Start by retrieving our original WndProc pointer
	HANDLE origProc = GetProp(hWnd, "_old_proc_");
	if (!origProc || IsWindowBeingDestroyed(hWnd))
		return DefWindowProc(hWnd, uMsg, wParam, lParam); /* We're basically fucked if we hit this :S */

	// Restore our original WndProc
	version(Win32) SetWindowLong(hWnd, GWL_WNDPROC, cast(LONG)origProc);
	version(Win64) SetWindowLongPtr(hWnd, GWL_WNDPROC, cast(LONG_PTR)origProc);
	
	// Remove the window property containing our original WndProc
	RemoveProp(hWnd, "_old_proc_");

	// Handle our message
	switch (uMsg) {
		case WM_GETMINMAXINFO:
			PMINMAXINFO minMaxInfo;
			RECT workArea;				RECT res;
			float ratio;
			LONG screenWidth;			LONG perMonitorWidth;
			LONG offsetTaskbarOnRight;
			RECT windowRect;			RECT clientRect;
			NCCALCSIZE_PARAMS params;
			LONG correctionOffset;
			POINT mouse;
			LONG newX;					LONG newWidth;
			LONG pixelShift = 1;

			// Do default handling first
			DefWindowProc(hWnd, uMsg, wParam, lParam);

			// Convert our message lParam into a minmaxinfo struct
			minMaxInfo = cast(PMINMAXINFO)lParam;

			// Get our current desktop area
			SystemParametersInfo(SPI_GETWORKAREA, 0, &workArea, 0);
			
			version(TestingOutput) {
				string out1 = "ptMaxSize.x=" ~ to!string(minMaxInfo.ptMaxSize.x)
					~ ", new value=" ~ to!string(GetSystemMetrics(SM_CXMAXIMIZED));
				string out2 = "ptMaxSize.y=" ~ to!string(minMaxInfo.ptMaxSize.y)
					~ ", new value=" ~ to!string(GetSystemMetrics(SM_CYMAXIMIZED));
				string out3 = "ptMaxPosition.x=" ~ to!string(minMaxInfo.ptMaxPosition.x)
					~ ", new value=" ~ to!string(((workArea.right - workArea.left) - minMaxInfo.ptMaxSize.x) / 2);
				string out4 = "ptMaxPosition.y=" ~ to!string(minMaxInfo.ptMaxPosition.y)
					~ ", new value=" ~ to!string(((workArea.bottom - workArea.top) - minMaxInfo.ptMaxSize.y) / 2);
				MessageBox(GetDesktopWindow(), to!string(out1 ~ out2 ~ out3 ~ out4).toWinStr(), "Message", 0);
			}

			// Set minMaxInfo fields to default values, rather than just handling them relatively
			minMaxInfo.ptMaxSize.x = GetSystemMetrics(SM_CXMAXIMIZED);
			minMaxInfo.ptMaxSize.y = GetSystemMetrics(SM_CYMAXIMIZED);
			minMaxInfo.ptMaxPosition.x = ((workArea.right - workArea.left) - minMaxInfo.ptMaxSize.x) / 2;
			minMaxInfo.ptMaxPosition.y = ((workArea.bottom - workArea.top) - minMaxInfo.ptMaxSize.y) / 2;
			
			// Get our current screen resolution
			GetWindowRect(GetDesktopWindow(), &res);

			// If our ratio is above 2:1, we're in 2-screen Surround setup. If above 4:1, we're in a 3-screen
			ratio = cast(float)res.right / cast(float)res.bottom;

			// Do setup calculations, etc (only if ratio > 2, otherwise we're doing it all for no reason)
			if (ratio > 2) {
				// Record screen width for easy use later
				screenWidth = res.right - res.left;

				// In case we're running with the Taskbar on the right, work out the necessary offset for it
				offsetTaskbarOnRight = res.right - workArea.right;
				
				// Processing to deal with non-standard WM_NCCALCSIZE handling
				GetWindowRect(hWnd, &windowRect);
				GetClientRect(hWnd, &clientRect);
				params.rgrc[0] = windowRect;
				params.rgrc[1] = windowRect;
				params.rgrc[2] = clientRect;
				SendMessage(hWnd, WM_NCCALCSIZE, TRUE, cast(LPARAM)&params);
				correctionOffset = ((windowRect.right - windowRect.left) - 
				                    (params.rgrc[0].right - params.rgrc[0].left)) / 2;

				// Get the mouse position (do it here to save time if we're not in Surround)
				GetCursorPos(&mouse);
			}

			// Process window sizing/pos
			if (ratio > 4) {
				// Store the width of an individual monitor
				perMonitorWidth = screenWidth / 3;
				
				// Calculate the new width
				newWidth = perMonitorWidth - correctionOffset;
				
				// Now calculate the new X coord
				if (mouse.x >= screenWidth - perMonitorWidth) {
					newX = minMaxInfo.ptMaxPosition.x + perMonitorWidth * 2 - minMaxInfo.ptMaxPosition.x
						- workArea.left + correctionOffset;
					newWidth -= minMaxInfo.ptMaxPosition.x + offsetTaskbarOnRight;
				} else if (mouse.x >= perMonitorWidth) {
					newX = minMaxInfo.ptMaxPosition.x + perMonitorWidth - minMaxInfo.ptMaxPosition.x - workArea.left
						+ correctionOffset;
					newWidth -= correctionOffset;
				} else {
					newX = minMaxInfo.ptMaxPosition.x;
					newWidth -= minMaxInfo.ptMaxPosition.x + workArea.left;
				}

				// Reposition the window
				minMaxInfo.ptMaxPosition.x = newX;
				minMaxInfo.ptMaxSize.x = newWidth;
				minMaxInfo.ptMaxSize.y = workArea.bottom - minMaxInfo.ptMaxPosition.y * 2 - workArea.top;
			} else if (ratio > 2) {
				// Store the width of an individual monitor
				perMonitorWidth = screenWidth / 2;
				
				// Calculate the new width
				newWidth = perMonitorWidth - minMaxInfo.ptMaxPosition.x - correctionOffset;
				
				// Now calculate the new X coord
				if (mouse.x >= perMonitorWidth) {
					newX = minMaxInfo.ptMaxPosition.x + perMonitorWidth - minMaxInfo.ptMaxPosition.x - workArea.left
						+ correctionOffset + pixelShift;
					newWidth -= offsetTaskbarOnRight + pixelShift;
				} else {
					newX = minMaxInfo.ptMaxPosition.x;
					newWidth -= workArea.left;
				}
				
				// Reposition the window
				minMaxInfo.ptMaxPosition.x = newX;
				minMaxInfo.ptMaxSize.x = newWidth;
				minMaxInfo.ptMaxSize.y = workArea.bottom - minMaxInfo.ptMaxPosition.y * 2 - workArea.top;
			}

			// Return 0 as per MSDN standards
			return 0;
		default:
			break;
	}

	// Otherwise, call our original WndProc
	return CallWindowProc(cast(WNDPROC)origProc, hWnd, uMsg, wParam, lParam);
}

/**
 * CallWndProc hook procedure to allow for message monitoring across a windows application
 * Params:
 * 	nCode = The code that the hook procedure uses to determine how to process the message
 * 	wParam = Depends on the nCode parameter
 * 	lParam = Depends on the nCode parameter
 * Returns: The value returned by the hook procedure determines whether the system allows or prevents one of these 
 * 	operations. The return value must be 0 to allow the operation, or 1 to prevent it
 */
export extern(Windows) LRESULT CallWndProc(int nCode, WPARAM wParam, LPARAM lParam) {
	PCWPSTRUCT msg;

	// Only process this message if we're allowed
	if (nCode >= HC_ACTION) {
		// Convert our hook lParam into a msg struct
		if (!lParam)
			return CallNextHookEx(null, nCode, wParam, lParam); /* Bail if the lParam happens to be NULL */
		msg = cast(PCWPSTRUCT)lParam;
		
		switch (msg.message) {
			case WM_GETMINMAXINFO:
				// Need to subclass the WndProc as we can't change the message values here
				if (!IsWindowBeingDestroyed(msg.hwnd)) {
					version(Win32) LONG proc = SetWindowLong(msg.hwnd, GWL_WNDPROC, cast(LONG)&WndProc);
					version(Win64) LONG_PTR proc = SetWindowLongPtr(msg.hwnd, GWL_WNDPROC, cast(LONG_PTR)&WndProc);
					SetProp(msg.hwnd, "_old_proc_", cast(HANDLE)proc);
				}
				break;
			case WM_SETCURSOR:
				// Handle this here because we can ;)
				if (!IsZoomed(msg.hwnd) && LOWORD(msg.lParam) == HTCAPTION && HIWORD(msg.lParam) == WM_MBUTTONUP) {
					RECT workArea;				RECT res;
					LONG screenWidth;			LONG perMonitorWidth;
					LONG offsetTaskbarOnBottom;	LONG offsetTaskbarOnRight;
					RECT windowRect;
					LONG wndWidth;				LONG wndHeight;
					LONG newX;
					POINT mouse;
					float ratio;

					// Get our current desktop area
					SystemParametersInfo(SPI_GETWORKAREA, 0, &workArea, 0);
					
					// Get our current screen resolution
					GetWindowRect(GetDesktopWindow(), &res);
					
					// Record screen width for easy use later
					screenWidth = res.right - res.left;

					// In case we're running with the Taskbar on the bottom, work out the necessary offset for it
					offsetTaskbarOnBottom = res.bottom - workArea.bottom;
					
					// In case we're running with the Taskbar on the right, work out the necessary offset for it
					offsetTaskbarOnRight = res.right - workArea.right;

					// Get the rect of the window, and calculate it's width/height
					GetWindowRect(msg.hwnd, &windowRect);
					wndWidth = windowRect.right - windowRect.left;
					wndHeight = windowRect.bottom - windowRect.top;

					// Get the mouse position (do it here to save time if we're not in Surround)
					GetCursorPos(&mouse);
					
					// Process window sizing/pos
					// If our ratio is above 2:1, we're in 2-screen Surround setup. If above 4:1, we're in a 3-screen
					ratio = cast(float)res.right / cast(float)res.bottom;
					if (ratio > 4) {
						// Store the width of an individual monitor
						perMonitorWidth = screenWidth / 3;

						// Now calculate the X coord
						if (mouse.x >= screenWidth - perMonitorWidth)
							newX = screenWidth - perMonitorWidth / 2 - wndWidth / 2 - offsetTaskbarOnRight / 2;
						else if (mouse.x >= perMonitorWidth)
							newX = perMonitorWidth + perMonitorWidth / 2 - wndWidth / 2;
						else
							newX = workArea.left / 2 + perMonitorWidth / 2 - wndWidth / 2;
					} else if (ratio > 2) {
						// Store the width of an individual monitor
						perMonitorWidth = screenWidth / 2;
						
						// Now calculate the X coord
						if (mouse.x >= perMonitorWidth)
							newX = screenWidth - perMonitorWidth / 2 - wndWidth / 2 - offsetTaskbarOnRight / 2;
						else
							newX = workArea.left / 2 + perMonitorWidth / 2 - wndWidth / 2;
					} else
						newX = workArea.left / 2 + screenWidth / 2 - wndWidth / 2 - offsetTaskbarOnRight / 2;

					// Reposition the window
					MoveWindow(msg.hwnd,
					           newX, 
					           workArea.top + (workArea.bottom - workArea.top) / 2 - wndHeight / 2,
					           wndWidth, 
					           wndHeight,
					           true);
				}
				break;
			default:
				break;
		}
	}

	// Call down the hook chain
	return CallNextHookEx(null, nCode, wParam, lParam);
}
