module main;

import core.runtime;
import std.conv;
import std.exception;
import std.string;

import eyemax.StrUtil;
import eyemax.WinAPI;

/**
 * Win32 Program Entrypoint for D
 * Params:
 * 	hInstance = A handle to the current instance of the application
 * 	hPrevInstance = A handle to the previous instance of the application. This parameter is always NULL
 * 	lpCmdLine = The command line for the application, excluding the program name
 * 	iCmdShow = Controls how the window is to be shown
 * Returns: If the function succeeds, terminating when it receives a WM_QUIT message, it should return the exit value 
 * 	contained in that message's wParam parameter. If the function terminates before entering the message loop, it 
 * 	should return zero
 */
extern(Windows) int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow) {
	int result;
	void exceptionHandler(Throwable e) { throw e; }

	// Init the D runtime
	try {
		Runtime.initialize(&exceptionHandler);
		result = DWinMain(hInstance, hPrevInstance, lpCmdLine, iCmdShow);
		Runtime.terminate(&exceptionHandler);
	} catch (Throwable e) {
		MessageBox(null, e.toString().toWinStr(), "Error", MB_OK | MB_ICONEXCLAMATION);
		result = 0;
	}
	
	return result;
}

/**
 * Druntime-wrapped Windows main application code
 * Params:
 * 	hInstance = A handle to the current instance of the application
 * 	hPrevInstance = A handle to the previous instance of the application. This parameter is always NULL
 * 	lpCmdLine = The command line for the application, excluding the program name
 * 	iCmdShow = Controls how the window is to be shown
 * Returns: If the function succeeds, terminating when it receives a WM_QUIT message, it should return the exit value 
 * 	contained in that message's wParam parameter. If the function terminates before entering the message loop, it 
 * 	should return zero
 */
int DWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow) {
	LPCTSTR cmdLine;
	uint cmdLineLen;
	HMODULE hDll;
	FARPROC hookProcAddr;
	HHOOK hHook;
	MSG msg;

	// Ensure we're being run from the main launcher, as opposed to directly
	cmdLine = GetCommandLine();
	version(Unicode) {
		cmdLineLen = cast(uint)wcslen(cmdLine);
	} else {
		cmdLineLen = strlen(cmdLine);
	}
	if (cmdLine[cmdLineLen-11..cmdLineLen] != "-safelaunch") {
		MessageBox(null, "This loader executable cannot be run directly. Please use the main program to run it.",
		            "Launch Error", MB_OK | MB_ICONEXCLAMATION);
		return cast(int)msg.wParam;
	}

	// ****** Load and install the hook procedure contained in HookLib.dll ******

	// Load our hook dll (but unload it on exit)
	scope(exit) FreeLibrary(hDll);
	version(Win32) {
		hDll = LoadLibrary("HookLib.x86.dll");
		if (hDll is null)
			throw new Exception("Unable to load HookLib.x86.dll. The program will now exit.");
	}
	version(Win64) {
		hDll = LoadLibrary("HookLib.x64.dll");
		if (hDll is null)
			throw new Exception("Unable to load HookLib.x64.dll. The program will now exit.");
	}
	
	// Get the address of our hook procedure
	hookProcAddr = GetProcAddress(hDll, "CallWndProc"); 
	version(Win32) {
		if (hookProcAddr is null)
			throw new Exception("Unable to find hook procedure in HookLib.x86.dll. The program will now exit.");
	}
	version(Win64) {
		if (hookProcAddr is null)
			throw new Exception("Unable to find hook procedure in HookLib.x64.dll. The program will now exit.");
	}
	
	// Install the hook (but remove it on exit)
	scope(exit) PostMessage(cast(HWND)HWND_BROADCAST, WM_NULL, 0, 0);
	scope(exit) UnhookWindowsHookEx(hHook);
	hHook = SetWindowsHookEx(WH_CALLWNDPROC, cast(HOOKPROC)hookProcAddr, hDll, 0);
	if (hHook is null)
		throw new Exception("Unable to install hook. The program will now exit.");

	// ****** Finally, enter our message loop while the hook works in the background ******

	// Main message loop for our app
	while (GetMessage(&msg, null, 0, 0)) {
		TranslateMessage(&msg);
		if (msg.message == WM_UNLOADHOOK)
			break;
	}

	return cast(int)msg.wParam;
}
