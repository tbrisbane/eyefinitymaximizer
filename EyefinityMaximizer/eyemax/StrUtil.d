module eyemax.StrUtil;

import std.string;
import std.utf;

import eyemax.WinAPI;

version(Unicode) {
	/**
	 * Convert a standard string into a 16-bit, null-terminated character array
	 * Params:
	 * 	str = The string to convert
	 * Returns: A 16-bit, null-terminated character array
	 */
	LPCTSTR toWinStr(string str) {
		return str.toUTF16z;
	}

	/**
	 * Convert a wide string into a 16-bit, null-terminated character array
	 * Params:
	 * 	str = The string to convert
	 * Returns: A 16-bit, null-terminated character array
	 */
	LPCTSTR toWinStr(wstring str) {
		return str.toUTF16z;
	}
} else {
	/**
	 * Convert a standard string into an 8-bit, null-terminated character array
	 * Params:
	 * 	str = The string to convert
	 * Returns: An 8-bit, null-terminated character array
	 */
	LPCTSTR toWinStr(string str) {
		return str.toStringz;
	}
}