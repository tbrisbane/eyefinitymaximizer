module eyemax.WinAPI;

pragma(lib, "Shell32.lib");

/// Win32 alias types
alias char CHAR;
alias CHAR*         LPCH,  LPSTR,  PCH,  PSTR;
alias const(CHAR)*  LPCCH, LPCSTR, PCCH, PCSTR;

alias wchar WCHAR;
alias WCHAR*        LPWCH,  LPWSTR,  PWCH,  PWSTR;
alias const(WCHAR)* LPCWCH, LPCWSTR, PCWCH, PCWSTR;

version(Unicode) {
	alias WCHAR			TCHAR;
	alias WCHAR*		LPTCH,	LPTSTR,		PTCH,	PTSTR;
	alias const(WCHAR)*	LPCTCH,	LPCTSTR,	PCTCH,	PCTSTR;
	alias WNDCLASSW		WNDCLASS;
} else {
	alias CHAR			TCHAR;
	alias CHAR*         LPTCH,	LPTSTR,		PTCH,	PTSTR;
	alias const(CHAR)*  LPCTCH,	LPCTSTR,	PCTCH,	PCTSTR;
	alias WNDCLASSA		WNDCLASS;
}

alias void* HANDLE;
alias void* PVOID, LPVOID;
alias HANDLE HINSTANCE;
alias HINSTANCE HMODULE;
alias HANDLE HWND;
alias HANDLE HHOOK;
alias HANDLE HMENU;
alias HANDLE HGDIOBJ;
alias HANDLE HBRUSH;
alias HANDLE HICON;
alias HICON HCURSOR;
alias ubyte BYTE;
alias ushort WORD;
alias int BOOL;
alias int LONG;
alias uint UINT;
alias uint DWORD;
alias uint ULONG;
version(Win64) {
	alias  long INT_PTR;
	alias ulong UINT_PTR;
	alias  long LONG_PTR;
}
version(Win32) {
	alias  int INT_PTR;
	alias uint UINT_PTR;
	alias  int LONG_PTR;
}
alias BYTE* LPBYTE;
alias WORD ATOM;
alias LONG HRESULT;
alias UINT_PTR WPARAM;
alias LONG_PTR LPARAM;
alias LONG_PTR LRESULT;
extern(Windows) {
	alias LRESULT function (HWND, UINT, WPARAM, LPARAM) WNDPROC;
	alias LRESULT function(int code, WPARAM wParam, LPARAM lParam) HOOKPROC;
	version(Win64) alias INT_PTR function() FARPROC;
	version(Win32) alias int function() FARPROC;
}

/// Win32 consts
enum : uint {
	// Win32 BOOL
	FALSE = 0,
	TRUE = 1,
	
	// MessageBox flags
	MB_OK = 0x00000000,
	MB_ICONEXCLAMATION = 0x00000030,
	
	// WaitForSingleObject flags
	INFINITE = uint.max,
	
	// WndClass style flags
	CS_VREDRAW = 0x0001,
	CS_HREDRAW = 0x0002,
	CS_KEYCVTWINDOW = 0x0004,
	CS_DBLCLKS = 0x0008,
	CS_OWNDC = 0x0020,
	CS_CLASSDC = 0x0040,
	CS_PARENTDC = 0x0080,
	CS_NOKEYCVT = 0x0100,
	CS_NOCLOSE = 0x0200,
	CS_SAVEBITS = 0x0800,
	CS_BYTEALIGNCLIENT = 0x1000,
	CS_BYTEALIGNWINDOW = 0x2000,
	CS_GLOBALCLASS = 0x4000,
	
	// LoadIcon/Cursor flags
	IDI_APPLICATION = 32512,
	IDC_ARROW = 32512,
	
	// GetStockObject flags
	WHITE_BRUSH = 0,
	
	// Window style flags
	WS_OVERLAPPED =       0x00000000,
	WS_POPUP =            0x80000000,
	WS_CHILD =            0x40000000,
	WS_MINIMIZE =         0x20000000,
	WS_VISIBLE =          0x10000000,
	WS_DISABLED =         0x08000000,
	WS_CLIPSIBLINGS =     0x04000000,
	WS_CLIPCHILDREN =     0x02000000,
	WS_MAXIMIZE =         0x01000000,
	WS_CAPTION =          0x00C00000,  /* WS_BORDER | WS_DLGFRAME  */
	WS_BORDER =           0x00800000,
	WS_DLGFRAME =         0x00400000,
	WS_VSCROLL =          0x00200000,
	WS_HSCROLL =          0x00100000,
	WS_SYSMENU =          0x00080000,
	WS_THICKFRAME =       0x00040000,
	WS_GROUP =            0x00020000,
	WS_TABSTOP =          0x00010000,
	WS_MINIMIZEBOX =      0x00020000,
	WS_MAXIMIZEBOX =      0x00010000,
	WS_TILED =            WS_OVERLAPPED,
	WS_ICONIC =           WS_MINIMIZE,
	WS_SIZEBOX =          WS_THICKFRAME,
	WS_OVERLAPPEDWINDOW = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX),
	WS_TILEDWINDOW =      WS_OVERLAPPEDWINDOW,
	WS_POPUPWINDOW =      (WS_POPUP | WS_BORDER | WS_SYSMENU),
	WS_CHILDWINDOW =      (WS_CHILD),
	
	// CreateWindow flags
	CW_USEDEFAULT = 0x80000000,
	HWND_DESKTOP = 0,
	
	// NotifyIconData size constraint
	NOTIFYICONDATA_V3_SIZE = 504,
	
	// NotifyIconData flags
	NIF_MESSAGE = 0x1,
	NIF_ICON = 0x2,
	NIF_TIP = 0x4,
	
	// Shell_NotifyIcon flags
	NIM_ADD = 0,
	NIM_DELETE = 0x2,
	
	// AppendMenu flags
	MF_BYPOSITION = 1024,
	MF_REMOVE = 4096,
	MF_SEPARATOR = 2048,
	MF_STRING = 0,
	
	// CheckMenu flags
	MF_BYCOMMAND = 0,
	MF_UNCHECKED = 0,
	MF_CHECKED = 8,
	
	// TrackPopupMenu flags
	TPM_LEFTALIGN = 0x0,
	TPM_CENTERALIGN = 0x4,
	TPM_RIGHTALIGN = 0x8,
	TPM_TOPALIGN = 0x0,
	TPM_VCENTERALIGN = 0x10,
	TPM_BOTTOMALIGN = 0x20,
	TPM_HORIZONTAL = 0x0,
	TPM_VERTICAL = 0x40,
	TPM_RETURNCMD = 0x100,
	TPM_NOANIMATION = 0x4000,
	
	// Hook types
	WH_CALLWNDPROC = 4,
	
	// HWND_BROADCAST message destination
	HWND_BROADCAST = 0xFFFF,
	
	// Messages
	WM_NULL = 0x0000,
	WM_DESTROY = 0x0002,
	WM_SETCURSOR = 0x0020,
	WM_GETMINMAXINFO = 0x0024,
	WM_NCCALCSIZE = 0x0083,
	WM_COMMAND = 0x0111,
	WM_RBUTTONUP = 0x0205,
	WM_MBUTTONUP = 0x0208,
	WM_APP = 0x8000,
	WM_SYSTRAYICON = WM_APP + 0,
	WM_UNLOADHOOK = WM_APP + 1,
	
	// DLL flags
	DLL_PROCESS_ATTACH = 1,
	DLL_THREAD_ATTACH = 2,
	DLL_THREAD_DETACH = 3,
	DLL_PROCESS_DETACH = 0,
	
	// GetWindowLongPtr flags
	GWL_WNDPROC = -4,
	GWL_EXSTYLE = -20,
	GWL_STYLE = -16,
	
	// CallWndProc flags
	HC_ACTION = 0,
	
	// WM_SETCURSOR flags
	HTCAPTION = 2,
	
	// LOGFONT flag
	LF_FACESIZE = 32,
	
	// SystemParametersInfo flags
	SPI_GETWORKAREA = 0x0030,
	SPI_GETNONCLIENTMETRICS = 0x0029,
	
	// GetSystemMetrics flags
	SM_CXMAXIMIZED = 61,
	SM_CYMAXIMIZED = 62,
}

extern(Windows) {
	/// Win32 MSG struct
	struct MSG {
		HWND        hwnd;
		UINT        message;
		WPARAM      wParam;
		LPARAM      lParam;
		DWORD       time;
		POINT       pt;
	}
	alias MSG* PMSG, NPMSG, LPMSG;

	/// Win32 SECURITY_ATTRIBUTES struct
	struct SECURITY_ATTRIBUTES {
		DWORD nLength;
		LPVOID lpSecurityDescriptor;
		BOOL bInheritHandle;
	}
	alias SECURITY_ATTRIBUTES* PSECURITY_ATTRIBUTES, LPSECURITY_ATTRIBUTES;

	/// Win32 STARTUPINFO struct
	struct STARTUPINFO {
		DWORD  cb = STARTUPINFO.sizeof;
		LPSTR  lpReserved;
		LPSTR  lpDesktop;
		LPSTR  lpTitle;
		DWORD  dwX;
		DWORD  dwY;
		DWORD  dwXSize;
		DWORD  dwYSize;
		DWORD  dwXCountChars;
		DWORD  dwYCountChars;
		DWORD  dwFillAttribute;
		DWORD  dwFlags;
		WORD   wShowWindow;
		WORD   cbReserved2;
		LPBYTE lpReserved2;
		HANDLE hStdInput;
		HANDLE hStdOutput;
		HANDLE hStdError;
	}
	alias STARTUPINFO *LPSTARTUPINFO;

	/// Win32 STARTUPINFO_W struct
	struct STARTUPINFO_W {
		DWORD  cb = STARTUPINFO_W.sizeof;
		LPWSTR lpReserved;
		LPWSTR lpDesktop;
		LPWSTR lpTitle;
		DWORD  dwX;
		DWORD  dwY;
		DWORD  dwXSize;
		DWORD  dwYSize;
		DWORD  dwXCountChars;
		DWORD  dwYCountChars;
		DWORD  dwFillAttribute;
		DWORD  dwFlags;
		WORD   wShowWindow;
		WORD   cbReserved2;
		LPBYTE lpReserved2;
		HANDLE hStdInput;
		HANDLE hStdOutput;
		HANDLE hStdError;
	}
	alias STARTUPINFO_W *LPSTARTUPINFO_W;

	/// Win32 PROCESS_INFORMATION struct
	struct PROCESS_INFORMATION {
		HANDLE hProcess;
		HANDLE hThread;
		DWORD  dwProcessId;
		DWORD  dwThreadId;
	}
	alias PROCESS_INFORMATION *LPPROCESS_INFORMATION;

	/// Win32 GUID struct
	struct GUID {
		DWORD   Data1;
		WORD    Data2;
		WORD    Data3;
		BYTE[8] Data4;
	}

	/// System Tray icon data struct
	struct NOTIFYICONDATA {
		DWORD      cbSize;
		HWND       hWnd;
		UINT       uID;
		UINT       uFlags;
		UINT       uCallbackMessage;
		HICON      hIcon;
		TCHAR[64]  szTip;
		DWORD      dwState;
		DWORD      dwStateMask;
		TCHAR[256] szInfo;
		union {
			UINT   uTimeout;
			UINT   uVersion;
		};
		TCHAR[64]  szInfoTitle;
		DWORD      dwInfoFlags;
		GUID       guidItem;
		HICON      hBalloonIcon;
	}
	alias NOTIFYICONDATA* PNOTIFYICONDATA;

	/// Ansi WindowClass
	struct WNDCLASSA {
		UINT        style;
		WNDPROC     lpfnWndProc;
		int         cbClsExtra;
		int         cbWndExtra;
		HINSTANCE   hInstance;
		HICON       hIcon;
		HCURSOR     hCursor;
		HBRUSH      hbrBackground;
		LPCSTR     lpszMenuName;
		LPCSTR     lpszClassName;
	}
	alias WNDCLASSA* PWNDCLASSA, NPWNDCLASSA, LPWNDCLASSA;
	
	/// Unicode WindowClass
	struct WNDCLASSW {
		UINT        style;
		WNDPROC     lpfnWndProc;
		int         cbClsExtra;
		int         cbWndExtra;
		HINSTANCE   hInstance;
		HICON       hIcon;
		HCURSOR     hCursor;
		HBRUSH      hbrBackground;
		LPCWSTR     lpszMenuName;
		LPCWSTR     lpszClassName;
	}
	alias WNDCLASSW* PWNDCLASSW, NPWNDCLASSW, LPWNDCLASSW;
	
	/// Win32 CWP hook struct
	struct CWPSTRUCT {
		LPARAM lParam;
		WPARAM wParam;
		UINT   message;
		HWND   hwnd;
	}
	alias CWPSTRUCT* PCWPSTRUCT, LPCWPSTRUCT;

	/// Win32 POINT struct
	struct POINT {
		LONG  x;
		LONG  y;
	}
	alias POINT* PPOINT, NPPOINT, LPPOINT;

	/// Win32 RECT struct
	struct RECT {
		LONG    left;
		LONG    top;
		LONG    right;
		LONG    bottom;
	}
	alias RECT* PRECT, NPRECT, LPRECT;

	/// Win32 MINMAXINFO struct
	struct MINMAXINFO {
		POINT ptReserved;
		POINT ptMaxSize;
		POINT ptMaxPosition;
		POINT ptMinTrackSize;
		POINT ptMaxTrackSize;
	}
	alias MINMAXINFO* PMINMAXINFO, LPMINMAXINFO;

	/// Win32 NCCALCSIZE_PARAMS struct
	struct NCCALCSIZE_PARAMS {
		RECT	rgrc[3];
		void*	lppos; /* We don't really need this, so just a void* for now */
	}
	alias NCCALCSIZE_PARAMS* LPNCCALCSIZE_PARAMS;

	/// Win32 LOGFONTA struct
	struct LOGFONTA {
		LONG lfHeight;
		LONG lfWidth;
		LONG lfEscapement;
		LONG lfOrientation;
		LONG lfWeight;
		BYTE lfItalic;
		BYTE lfUnderline;
		BYTE lfStrikeOut;
		BYTE lfCharSet;
		BYTE lfOutPrecision;
		BYTE lfClipPrecision;
		BYTE lfQuality;
		BYTE lfPitchAndFamily;
		CHAR[LF_FACESIZE] lfFaceName;
	}
	alias LOGFONTA* PLOGFONTA, NPLOGFONTA, LPLOGFONTA;

	/// Win32 LOGFONTW struct
	struct LOGFONTW {
		LONG lfHeight;
		LONG lfWidth;
		LONG lfEscapement;
		LONG lfOrientation;
		LONG lfWeight;
		BYTE lfItalic;
		BYTE lfUnderline;
		BYTE lfStrikeOut;
		BYTE lfCharSet;
		BYTE lfOutPrecision;
		BYTE lfClipPrecision;
		BYTE lfQuality;
		BYTE lfPitchAndFamily;
		WCHAR[LF_FACESIZE] lfFaceName;
	}
	alias LOGFONTW* PLOGFONTW, NPLOGFONTW, LPLOGFONTW;

	/// Win32 NONCLIENTMETRICSA struct
	struct NONCLIENTMETRICSA {
		UINT cbSize = this.sizeof;
		int iBorderWidth;
		int iScrollWidth;
		int iScrollHeight;
		int iCaptionWidth;
		int iCaptionHeight;
		LOGFONTA lfCaptionFont;
		int iSmCaptionWidth;
		int iSmCaptionHeight;
		LOGFONTA lfSmCaptionFont;
		int iMenuWidth;
		int iMenuHeight;
		LOGFONTA lfMenuFont;
		LOGFONTA lfStatusFont;
		LOGFONTA lfMessageFont;
	}
	alias NONCLIENTMETRICSA* LPNONCLIENTMETRICSA;

	/// Win32 NONCLIENTMETRICSW struct
	struct NONCLIENTMETRICSW {
		UINT cbSize = this.sizeof;
		int iBorderWidth;
		int iScrollWidth;
		int iScrollHeight;
		int iCaptionWidth;
		int iCaptionHeight;
		LOGFONTW lfCaptionFont;
		int iSmCaptionWidth;
		int iSmCaptionHeight;
		LOGFONTW lfSmCaptionFont;
		int iMenuWidth;
		int iMenuHeight;
		LOGFONTW lfMenuFont;
		LOGFONTW lfStatusFont;
		LOGFONTW lfMessageFont;
	}
	alias NONCLIENTMETRICSW* LPNONCLIENTMETRICSW;
}

/// Win32 function prototypes
extern(Windows) nothrow {
	int MessageBoxA(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);
	int MessageBoxW(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType);
	BOOL PostThreadMessageA(DWORD idThread, UINT Msg, WPARAM wParam, LPARAM lParam);
	BOOL PostThreadMessageW(DWORD idThread, UINT Msg, WPARAM wParam, LPARAM lParam);
	BOOL CreateProcessA(LPCSTR lpApplicationName, LPSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes,
	                    LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, 
	                    LPVOID lpEnvironment, LPCSTR lpCurrentDirectory, LPSTARTUPINFO lpStartupInfo, 
	                    LPPROCESS_INFORMATION lpProcessInformation);
	BOOL CreateProcessW(LPCWSTR lpApplicationName, LPWSTR lpCommandLine, LPSECURITY_ATTRIBUTES lpProcessAttributes,
	                    LPSECURITY_ATTRIBUTES lpThreadAttributes, BOOL bInheritHandles, DWORD dwCreationFlags, 
	                    LPVOID lpEnvironment, LPCWSTR lpCurrentDirectory, LPSTARTUPINFO_W lpStartupInfo, 
	                    LPPROCESS_INFORMATION lpProcessInformation);
	BOOL CloseHandle(HANDLE hObject);
	DWORD WaitForSingleObject(HANDLE hHandle, DWORD dwMilliseconds);
	HICON LoadIconA(HINSTANCE hInstance, LPCSTR lpIconName);
	HICON LoadIconW(HINSTANCE hInstance, LPCWSTR lpIconName);
	HCURSOR LoadCursorA(HINSTANCE hInstance, LPCSTR lpCursorName);
	HCURSOR LoadCursorW(HINSTANCE hInstance, LPCWSTR lpCursorName);
	HGDIOBJ GetStockObject(int);
	BOOL UnregisterClassA(LPCTSTR lpClassName, HINSTANCE hInstance = null);
	BOOL UnregisterClassW(LPCWSTR lpClassName, HINSTANCE hInstance = null);
	ATOM RegisterClassA(const WNDCLASSA *lpWndClass);
	ATOM RegisterClassW(const WNDCLASSW *lpWndClass);
	BOOL DestroyWindow(HWND hWnd);
	HWND CreateWindowExA(DWORD dwExStyle, LPCSTR lpClassName, LPCSTR lpWindowName, DWORD dwStyle, int x, int y, 
	                     int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, 
	                     LPVOID lpParam);
	HWND CreateWindowExW(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int x, int y, 
	                     int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, 
	                     LPVOID lpParam);
	BOOL UpdateWindow(HWND hWnd);
	LPSTR MAKEINTRESOURCEA(int i) { return cast(LPSTR)(cast(DWORD)(cast(WORD)(i))); }
	LPWSTR MAKEINTRESOURCEW(int i) { return cast(LPWSTR)(cast(DWORD)(cast(WORD)(i))); }
	BOOL Shell_NotifyIconA(DWORD dwMessage, PNOTIFYICONDATA lpdata);
	BOOL Shell_NotifyIconW(DWORD dwMessage, PNOTIFYICONDATA lpdata);
	UINT RegisterWindowMessageA(LPCTSTR lpString);
	UINT RegisterWindowMessageW(LPCWSTR lpString);
	BOOL DestroyMenu(HMENU hMenu);
	HMENU CreatePopupMenu();
	BOOL AppendMenuA(HMENU hMenu, uint uFlags, uint uIDNewItem, LPCTSTR lpNewItem = null);
	BOOL AppendMenuW(HMENU hMenu, uint uFlags, uint uIDNewItem, LPCWSTR lpNewItem = null);
	BOOL PostMessageA(HWND hWnd, uint Msg, WPARAM wParam, LPARAM lParam);
	BOOL PostMessageW(HWND hWnd, uint Msg, WPARAM wParam, LPARAM lParam);
	BOOL GetMessageA(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax);
	BOOL GetMessageW(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax);
	BOOL TranslateMessage(MSG* lpMsg);
	LRESULT DispatchMessageA(const MSG *lpmsg);
	LRESULT DispatchMessageW(const MSG *lpmsg);
	void PostQuitMessage(int nExitCode);
	BOOL SetForegroundWindow(HWND hWnd);
	BOOL TrackPopupMenu(HMENU hMenu, UINT uFlags, int x, int y, int nReserved, HWND hWnd, RECT *prcRect);
	LRESULT DefWindowProcA(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	LRESULT DefWindowProcW(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	LPTSTR GetCommandLineA();
	LPWSTR GetCommandLineW();
	BOOL FreeLibrary(HMODULE hLibModule);
	HMODULE LoadLibraryA(LPCSTR lpLibFileName);
	HMODULE LoadLibraryW(LPCWSTR lpLibFileName);
	FARPROC GetProcAddress(HMODULE hModule, LPCSTR lpProcName);
	HHOOK SetWindowsHookExA(int idHook, HOOKPROC lpfn, HINSTANCE hMod, DWORD dwThreadId);
	HHOOK SetWindowsHookExW(int idHook, HOOKPROC lpfn, HINSTANCE hMod, DWORD dwThreadId);
	LRESULT CallNextHookEx(HHOOK hhk, int nCode, WPARAM wParam, LPARAM lParam);
	BOOL UnhookWindowsHookEx(HHOOK hhk);
	version(Win32) {
		LONG SetWindowLongA(HWND hWnd, int nIndex, LONG dwNewLong);
		LONG SetWindowLongW(HWND hWnd, int nIndex, LONG dwNewLong);
		LONG GetWindowLongA(HWND hWnd, int nIndex);
		LONG GetWindowLongW(HWND hWnd, int nIndex);
	}
	version(Win64) {
		LONG_PTR SetWindowLongPtrA(HWND hWnd, int nIndex, LONG_PTR dwNewLong);
		LONG_PTR SetWindowLongPtrW(HWND hWnd, int nIndex, LONG_PTR dwNewLong);
		LONG_PTR GetWindowLongPtrA(HWND hWnd, int nIndex);
		LONG_PTR GetWindowLongPtrW(HWND hWnd, int nIndex);
	}
	HANDLE SetPropA(HWND hWnd, LPCTSTR lpString, HANDLE hData);
	HANDLE SetPropW(HWND hWnd, LPCWSTR lpString, HANDLE hData);
	HANDLE GetPropA(HWND hWnd, LPCTSTR lpString);
	HANDLE GetPropW(HWND hWnd, LPCWSTR lpString);
	HANDLE RemovePropA(HWND hWnd, LPCTSTR lpString);
	HANDLE RemovePropW(HWND hWnd, LPCWSTR lpString);
	BOOL SystemParametersInfoA(UINT uiAction, UINT uiParam, PVOID pvParam, UINT fWinIni);
	BOOL SystemParametersInfoW(UINT uiAction, UINT uiParam, PVOID pvParam, UINT fWinIni);
	LRESULT SendMessageA(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	LRESULT SendMessageW(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	int GetSystemMetrics(int nIndex);
	HWND GetDesktopWindow();
	BOOL GetClientRect(HWND hWnd, LPRECT lpRect);
	BOOL GetWindowRect(HWND hWnd, LPRECT lpRect);
	BOOL AdjustWindowRectEx(LPRECT lpRect, DWORD dwStyle, BOOL bMenu, DWORD dwExStyle);
	BOOL GetCursorPos(LPPOINT lpPoint);
	WORD HIWORD(long x) { return cast(WORD)((x >> 16) & 0xFFFF); }
	WORD LOWORD(long x) { return cast(WORD)x; }
	LRESULT CallWindowProcA(WNDPROC lpPrevWndFunc, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	LRESULT CallWindowProcW(WNDPROC lpPrevWndFunc, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	BOOL IsZoomed(HWND hWnd);
	BOOL MoveWindow(HWND hWnd, int X, int Y, int nWidth, int nHeight, BOOL bRepaint);
	HMODULE GetModuleHandleA(LPCSTR lpModuleName);
	HMODULE GetModuleHandleW(LPCWSTR lpModuleName);
}

/// Win32 function aliases
version(Unicode) {
	alias MessageBoxW MessageBox;
	alias PostThreadMessageW PostThreadMessage;
	alias CreateProcessW CreateProcess;
	alias LoadIconW LoadIcon;
	alias LoadCursorW LoadCursor;
	alias UnregisterClassW UnregisterClass;
	alias RegisterClassW RegisterClass;
	alias CreateWindowExW CreateWindowEx;
	alias MAKEINTRESOURCEW MAKEINTRESOURCE;
	alias Shell_NotifyIconW Shell_NotifyIcon;
	alias RegisterWindowMessageW RegisterWindowMessage;
	alias AppendMenuW AppendMenu;
	alias PostMessageW PostMessage;
	alias GetMessageW GetMessage;
	alias DispatchMessageW DispatchMessage;
	alias DefWindowProcW DefWindowProc;
	alias GetCommandLineW GetCommandLine;
	alias LoadLibraryW LoadLibrary;
	alias SetWindowsHookExW SetWindowsHookEx;
	version(Win32) {
		alias SetWindowLongW SetWindowLong;
		alias GetWindowLongW GetWindowLong;
	}
	version(Win64) {
		alias SetWindowLongPtrW SetWindowLongPtr;
		alias GetWindowLongPtrW GetWindowLongPtr;
	}
	alias SetPropW SetProp;
	alias GetPropW GetProp;
	alias RemovePropW RemoveProp;
	alias SystemParametersInfoW SystemParametersInfo;
	alias SendMessageW SendMessage;
	alias CallWindowProcW CallWindowProc;
	alias GetModuleHandleW GetModuleHandle;
} else {
	alias MessageBoxA MessageBox;
	alias PostThreadMessageA PostThreadMessage;
	alias CreateProcessA CreateProcess;
	alias LoadIconA LoadIcon;
	alias LoadCursorA LoadCursor;
	alias UnregisterClassA UnregisterClass;
	alias RegisterClassA RegisterClass;
	alias CreateWindowExA CreateWindowEx;
	alias MAKEINTRESOURCEA MAKEINTRESOURCE;
	alias Shell_NotifyIconA Shell_NotifyIcon;
	alias RegisterWindowMessageA RegisterWindowMessage;
	alias AppendMenuA AppendMenu;
	alias PostMessageA PostMessage;
	alias GetMessageA GetMessage;
	alias DispatchMessageA DispatchMessage;
	alias DefWindowProcA DefWindowProc;
	alias GetCommandLineA GetCommandLine;
	alias LoadLibraryA LoadLibrary;
	alias SetWindowsHookExA SetWindowsHookEx;
	version(Win32) {
		alias SetWindowLongA SetWindowLong;
		alias GetWindowLongA GetWindowLong;
	}
	version(Win64) {
		alias SetWindowLongPtrA SetWindowLongPtr;
		alias GetWindowLongPtrA GetWindowLongPtr;
	}
	alias SetPropA SetProp;
	alias GetPropA GetProp;
	alias RemovePropA RemoveProp;
	alias SystemParametersInfoA SystemParametersInfo;
	alias SendMessageA SendMessage;
	alias CallWindowProcA CallWindowProc;
	alias GetModuleHandleA GetModuleHandle;
}

/// Win32 function pointers for dynamically loaded libraries
private alias extern(Windows) nothrow HRESULT function(BOOL *pfEnabled) DWMENABLED;
private DWMENABLED DwmIsCompositionEnabled;
private alias extern(Windows) nothrow BOOL function(HWND hwnd) WININDESTROY;
private WININDESTROY IsWindowInDestroy;

/** Wrapper function to check if the DWM is currently active
 * Returns: A bool indicating wether the DWM is currently active
 */
bool IsDwmEnabled() {
	BOOL bisDwmEnabled;
	return DwmIsCompositionEnabled && !DwmIsCompositionEnabled(&bisDwmEnabled) && bisDwmEnabled;
}

/** Wrapper function to check if a window is currently being destroyed
 * Params:
 * 	hwnd = The handle to the window to check
 * Returns: A bool indicating whether the window is currently being destroyed
 */
bool IsWindowBeingDestroyed(HWND hwnd) {
	return IsWindowInDestroy && IsWindowInDestroy(hwnd);
}

/// Static module initializer to be run during program startup
static this() {
	// Get function pointer for DwmIsCompositionEnabled
	auto hDwmapi = GetModuleHandle("dwmapi");
	DwmIsCompositionEnabled = cast(DWMENABLED)GetProcAddress(hDwmapi, "DwmIsCompositionEnabled");

	// Get the function pointer for the undocumented IsWindowInDestroy
	auto hUser32 = GetModuleHandle("user32");
	IsWindowInDestroy = cast(WININDESTROY)GetProcAddress(hUser32, "IsWindowInDestroy");
}
