module main;

import core.runtime;
import std.internal.windows.advapi32;
import std.string;
import std.utf;
import std.conv;
import std.exception;

import eyemax.StrUtil;
import eyemax.WinAPI;

/// Win32 TaskbarCreated message
UINT TASKBAR_RESET_MSG;

/// Win32 System Tray icon data
NOTIFYICONDATA niData;

/// Win32 PopupMenu consts
immutable string[] menuStrings = ["About...", "Exit"];
immutable int[] menuIds = [1, 2];

/// Win32 popup menu handle
HMENU popupMenu;

/**
 * Win32 Program Entrypoint for D
 * Params:
 * 	hInstance = A handle to the current instance of the application
 * 	hPrevInstance = A handle to the previous instance of the application. This parameter is always NULL
 * 	lpCmdLine = The command line for the application, excluding the program name
 * 	iCmdShow = Controls how the window is to be shown
 * Returns: If the function succeeds, terminating when it receives a WM_QUIT message, it should return the exit value 
 * 	contained in that message's wParam parameter. If the function terminates before entering the message loop, it 
 * 	should return zero
 */
extern(Windows) int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow) {
	int result;
	void exceptionHandler(Throwable e) { throw e; }

	// Init the D runtime
	try {
		Runtime.initialize(&exceptionHandler);
		result = DWinMain(hInstance, hPrevInstance, lpCmdLine, iCmdShow);
		Runtime.terminate(&exceptionHandler);
	} catch (Throwable e) {
		MessageBox(null, e.toString().toWinStr(), "Error", MB_OK | MB_ICONEXCLAMATION);
		result = 0;
	}
	
	return result;
}

/**
 * Druntime-wrapped Windows main application code
 * Params:
 * 	hInstance = A handle to the current instance of the application
 * 	hPrevInstance = A handle to the previous instance of the application. This parameter is always NULL
 * 	lpCmdLine = The command line for the application, excluding the program name
 * 	iCmdShow = Controls how the window is to be shown
 * Returns: If the function succeeds, terminating when it receives a WM_QUIT message, it should return the exit value 
 * 	contained in that message's wParam parameter. If the function terminates before entering the message loop, it 
 * 	should return zero
 */
int DWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow) {
	// Init startup info for our HookLoaders
	version(Unicode) {
		STARTUPINFO_W si;
	} else {
		STARTUPINFO si;
	}
	PROCESS_INFORMATION pi32, pi64;
	version(Unicode) {
		wchar[] cmdLine = cast(wchar[])"-safelaunch\0";
		wchar[] caption = cast(wchar[])"Eyefinity Maximizer\0";
	} else {
		char[] cmdLine = cast(char[])"-safelaunch\0";
		char[] caption = cast(char[])"Eyefinity Maximizer\0";
	}
	string className = "EyeMaxClass";
	HWND hWnd;
	MSG msg;
	WNDCLASS wndClass;
	ATOM wndClassATOM;
	enum iconId = 12345;

	// ****** Run appropriate HookLoader(s) to install our HookLib ******

	// Set struct size member to size
	si.cb = si.sizeof;

	// Now create our loader process (but tell it to quit when we do!)
	scope(exit) {
		CloseHandle(pi32.hThread);
		CloseHandle(pi32.hProcess);
		WaitForSingleObject(pi32.hProcess, INFINITE);
		PostThreadMessage(pi32.dwThreadId, WM_UNLOADHOOK, 0, 0);
	}
	CreateProcess("HookLoader.x86.exe".toWinStr(), cmdLine.ptr, null, null, false, 0, null, null, &si, &pi32);
	if (pi32.hProcess is null)
		throw new Exception("Unable to launch HookLoader.x86.exe. The program will now exit.");

	// Load the 64 bit hook as well if we need to
	scope(exit) {
		if (isWow64) { /* Defined in advapi32 */
			CloseHandle(pi64.hThread);
			CloseHandle(pi64.hProcess);
			WaitForSingleObject(pi64.hProcess, INFINITE);
			PostThreadMessage(pi64.dwThreadId, WM_UNLOADHOOK, 0, 0);
		}
	}
	if (isWow64) {
		CreateProcess("HookLoader.x64.exe".toWinStr(), cmdLine.ptr, null, null, false, 0, null, null, &si, &pi64);
		if (pi64.hProcess is null)
			throw new Exception("Unable to launch HookLoader.x64.exe. The program will now exit.");
	}

	// ****** Create a new invisible window for interaction with a System Tray icon ******

	// Setup the Window class (and unregister it on exit)
	wndClass.style         = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc   = &WndProc;
	wndClass.cbClsExtra    = 0;
	wndClass.cbWndExtra    = 0;
	wndClass.hInstance     = hInstance;
	wndClass.hIcon         = LoadIcon(null, cast(LPCTSTR)IDI_APPLICATION);
	wndClass.hCursor       = LoadCursor(null, cast(LPCTSTR)IDC_ARROW);
	wndClass.hbrBackground = cast(HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName  = null;
	wndClass.lpszClassName = className.toWinStr();

	scope(exit) UnregisterClass(cast(LPCTSTR)wndClassATOM);
	wndClassATOM = RegisterClass(&wndClass);
	if (!wndClassATOM)
		throw new Exception("Couldn't register window class. The program will now exit.");

	// Create the window
	scope(exit) DestroyWindow(hWnd);
	hWnd = CreateWindowEx(0,							/* window extended style */
	                      cast(LPCTSTR)wndClassATOM,	/* window class name */
	                      caption.ptr,					/* window caption */
	                      WS_OVERLAPPED,				/* window style */
	                      CW_USEDEFAULT,				/* initial x position */
	                      CW_USEDEFAULT,				/* initial y position */
	                      0,							/* initial x size */
	                      0,							/* initial y size */
	                      cast(HWND)HWND_DESKTOP,		/* parent window handle */
	                      null,							/* window menu handle */
	                      hInstance,					/* program instance handle */
	                      null);						/* creation parameters */
	if (hWnd is null)
		throw new Exception("Couldn't create window.");

	// Update it
	UpdateWindow(hWnd);

	// ****** Create a new System Tray icon so we can actually exit the program ******

	// Create the System Tray icon data
	niData.cbSize = NOTIFYICONDATA_V3_SIZE;
	niData.uID = iconId;
	niData.uFlags = NIF_MESSAGE|NIF_ICON|NIF_TIP;
	niData.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(1000)); /* Use app icon for the Sys-Tray icon */
	niData.szTip = caption ~ niData.szTip[20..64];
	niData.hWnd = hWnd;
	niData.uCallbackMessage = WM_SYSTRAYICON;

	// Add our icon to the System Tray (but remove it on exit)
	scope(exit) Shell_NotifyIcon(NIM_DELETE, &niData);
	Shell_NotifyIcon(NIM_ADD, &niData);

	// Register a message callback for when explorer restarts
	TASKBAR_RESET_MSG = RegisterWindowMessage("TaskbarCreated");

	// ****** Create the popup menu the System Tray icon will use to interact with the user ******

	// Create our popup menu, and populate it with items (but remove it on exit)
	scope(exit) DestroyMenu(popupMenu);
	popupMenu = CreatePopupMenu();
	for (int i=0; i<menuIds.length; i++)
		AppendMenu(popupMenu, MF_STRING, menuIds[i], menuStrings[i].toWinStr());

	// ****** Finally, enter our message loop while the hook works in the background ******

	// Main message loop for our app
	while (GetMessage(&msg, null, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

/**
 * The Window Procedure for handling all messages sent to our app
 * Params:
 * 	hWnd = The window handle to which the given message applies
 * 	message = The window message to process
 * 	wParam = The wide parameter for this message
 * 	lParam = The long parameter for this message
 * Returns: The result of processing the given message
 */
extern(Windows) LRESULT WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) nothrow {
	// Handle messages
	switch (message) {
		case WM_COMMAND: /* Handle comments from our popup menu */
			// First, make sure we're getting a command from a menu
			if (HIWORD(wParam) != 0)
				return 0;

			// Now, handle our selections
			switch (LOWORD(wParam)) {
				case 1:
					MessageBox(hWnd, "Eyefinity Maximizer\n\n"
					            "Copyright © 2013 Trey Brisbane", "About Eyefinity Maximizer", 0);
					return 0;
				case 2:
					PostQuitMessage(0);
					return 0;
				default:
					return 0;
			}
		case WM_DESTROY: /* Handle closing the main window */
			PostQuitMessage(0);
			return 0;
		case WM_SYSTRAYICON: /* Handle interaction with the Sys Tray icon */
			switch (lParam) {
				case WM_RBUTTONUP:
					// Get mouse position first
					POINT mouse;
					if (!GetCursorPos(&mouse)) {
						mouse.x = 0;
						mouse.y = 0;
					}

					// Now open our menu
					SetForegroundWindow(hWnd); /* Hack to fix menu not closing */
					TrackPopupMenu(popupMenu, TPM_LEFTALIGN | TPM_TOPALIGN | TPM_NOANIMATION, mouse.x, mouse.y, 0, 
					               hWnd, null);
					PostMessage(hWnd, WM_NULL, 0, 0); /* Hack to fix menu closing when it shouldn't */
					return 0;
				default:
					return 0;
			}
		case TASKBAR_RESET_MSG:
			Shell_NotifyIcon(NIM_ADD, &niData);
			break;
		default:
			break;
	}

	// Then do whatever the default is for it
	return DefWindowProc(hWnd, message, wParam, lParam);
}
